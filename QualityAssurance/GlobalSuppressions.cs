//-----------------------------------------------------------------------
// <author> 
//    auto-generated
// </author>
//
// <date> 
//     11th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="GlobalSuppressions.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.    
// </summary>
//-----------------------------------------------------------------------

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Justification = "Written for specific compiler.", Scope = "member", Target = "Masti.QualityAssurance.MastiDiagnostics.#LogInfo(System.String,System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Justification = "Written for specific compiler.", Scope = "member", Target = "Masti.QualityAssurance.MastiDiagnostics.#LogWarning(System.String,System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Justification = "Written for specific compiler.", Scope = "member", Target = "Masti.QualityAssurance.MastiDiagnostics.#LogSuccess(System.String,System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed", Justification = "Written for specific compiler.", Scope = "member", Target = "Masti.QualityAssurance.MastiDiagnostics.#LogError(System.String,System.String,System.String)")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "System-generated file.", Scope = "type", Target = "QualityAssurance.Properties.Settings")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Method returns bool status. Caller can use this. Telemetry is a secondary affair. Shouldn't affect main application.", Scope = "member", Target = "Masti.QualityAssurance.TelemetryCollector.#StoreTelemetry()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Method returns bool status. Caller can use this. Telemetry is a secondary affair. Shouldn't affect main application.", Scope = "member", Target = "Masti.QualityAssurance.TelemetryCollector.#RegisterTelemetry(System.String,Masti.QualityAssurance.ITelemetry)")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Will deal with once other modules implement this interface.", Scope = "member", Target = "Masti.QualityAssurance.ITelemetry.#DataCapture")]

