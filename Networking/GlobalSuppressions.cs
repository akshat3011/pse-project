﻿// <author> 
//      Ayush Mittal(29ayush@gmail.com)
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
// </reviewer>
//
// <copyright file="Helper.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
//      You are allowed to use the file and/or redistribute/modify as long as you preserve this copyright header and author tag.C:\Users\PsychoWarrior\Source\Repos\pse-project\Networking\UnitTests\DataRecevialNotifierTest.cs
// </copyright>
//
// <summary>
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
// </summary>
// -----------------------------------------------------------------------


[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Run over a lan, hence no need to specify locale.", Scope = "member", Target = "~M:Masti.Networking.Communication.InitiateConnection~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Run over a lan, hence no need to specify locale.", Scope = "member", Target = "~M:Masti.Networking.Communication.SendCallback(System.IAsyncResult)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Run over a lan, hence no need to specify locale.", Scope = "member", Target = "~M:Masti.Networking.Helper.MD5Hash(System.String)~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Security", "CA5351:Do Not Use Broken Cryptographic Algorithms", Justification = "Only used for hashing purposes not security", Scope = "member", Target = "~M:Masti.Networking.Helper.MD5Hash(System.String)~System.String")]// -----------------------------------------------------------------------
